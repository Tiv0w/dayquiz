# dayquiz

Your task is simple : given a random date, you must answer the corresponding day of the week.


This is a small game I made to train on the Doomsday algorithm (and train on writing Crystal code).


## Usage

```
Usage: dayquiz [options]
    -i, --infinite                   Play the game continuously
    -t, --timed                      Display the time it took to answer
    -r RANGE, --range=RANGE          Specifies the year range (default: 1800-2100)
    -l YEAR, --lower=YEAR            Specifies the lower bound year (default: 1800)
    -u YEAR, --upper=YEAR            Specifies the upper bound year (default: 2100)
    -c, --compact                    Print instructions in compact format
    -v DATE, --verify=DATE           Verify a given date (formatted as `d/m/yyyy`)
    -h, --help                       Show this help
```
