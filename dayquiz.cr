require "option_parser"

infinite = false
compact = false
timed = false
lower_bound_year = 1800
upper_bound_year = 2100
OptionParser.parse do |parser|
  parser.banner = "Usage: dayquiz [options]"
  parser.on("-i", "--infinite", "Play the game continuously") { infinite = true }
  parser.on("-t", "--timed", "Display the time it took to answer") { timed = true }
  parser.on("-r RANGE", "--range=RANGE", "Specifies the year range (default: 1800-2100)") do |range|
    lower_bound_year, upper_bound_year = parse_year_range range
  end
  parser.on("-l YEAR", "--lower=YEAR", "Specifies the lower bound year (default: 1800)") { |year| lower_bound_year = year.to_i }
  parser.on("-u YEAR", "--upper=YEAR", "Specifies the upper bound year (default: 2100)") { |year| upper_bound_year = year.to_i }
  parser.on("-c", "--compact", "Print instructions in compact format") { compact = true }
  parser.on("-v DATE", "--verify=DATE", "Verify a given date (formatted as `d/m/yyyy`)") do |date|
    if verify(date).nil?
      puts "Impossible to parse given date : #{date}. Please format it as `dd/mm/yyyy`."
      exit(1)
    else
      puts "#{date} is a #{verify date}."
      exit()
    end
  end
  parser.on("-h", "--help", "Show this help") do
    puts parser
    exit
  end
  parser.invalid_option do |flag|
    STDERR.puts "ERROR: #{flag} is not a valid option"
    STDERR.puts parser
    exit(1)
  end
end


def parse_year_range(year_range : String) : Tuple(Int32, Int32)
  if year_range.matches?(/^\d{1,4}-\d{4}$/)
    lower, upper = year_range.split('-').map { |elem| elem.to_i }
    return {lower, upper}
  else
    return {1800, 2100}
  end
end


def verify(date : String) : String | Nil
  if date.matches?(/^\d{1,2}\/\d{1,2}\/\d{4}$/)
    day, month, year = date.split('/').map { |x| x.to_i }
    time = Time.utc(year, month, day)
    val = time.day_of_week.value

    formatter = Time::Format.new "%A (#{val == 7 ? "0 or 7" : val})"
    return formatter.format(time)
  else
    return nil
  end
end


def generate_date(lower_bound_year : Int32, upper_bound_year : Int32) : Time
  lower_bound_timestamp : Int64 = Time.utc(lower_bound_year, 1, 1).to_unix
  upper_bound_timestamp : Int64 = Time.utc(upper_bound_year, 12, 31).to_unix

  chosen_timestamp = Random.rand(lower_bound_timestamp..upper_bound_timestamp)
  
  return Time.unix chosen_timestamp
end


def format_answer_time(t : Time::Span) : String
  str = ""
  if t.minutes != 0
    str += "#{t.minutes} min "
  end
  str += "#{t.seconds} sec #{t.milliseconds}"
  return str
end


enum Code
  Win
  Loss
  Incorrect
  Quit
end


def ask_question(lower_bound_year : Int32, upper_bound_year : Int32, compact : Bool, timed : Bool) : Code
  readable_formatter = Time::Format.new "%B %-d %Y"
  compact_formatter = Time::Format.new "%-d / %-m / %Y"
  date = generate_date lower_bound_year, upper_bound_year
  answer : Time::DayOfWeek = date.day_of_week
  val = answer.value

  win_msg = ""
  loss_msg = ""
  if compact
    printf("%s : ", compact_formatter.format(date))
    win_msg = "Correct !"
    loss_msg = "Wrong ! It is a #{answer} (#{val == 7 ? "0 or 7" : val})."
  else
    puts "What day of the week corresponds to #{readable_formatter.format(date)}?    ->    #{compact_formatter.format(date)}"
    win_msg = "Correct ! #{readable_formatter.format(date)} is a #{answer} (#{val}). 🥳"
    loss_msg = "Wrong ! #{readable_formatter.format(date)} is a #{answer} (#{val}). 🥲"
  end

  start_time = Time.monotonic

  guess = gets
  if guess.nil? || guess.blank?
    return Code::Quit
  end

  answer_time = Time.monotonic - start_time

  result = nil
  guess = guess.strip
  if guess.downcase == 'q'
    return Code::Quit
  end
  begin
    guess = guess.to_i
    result = val == guess || val % 7 == guess
  rescue
    if guess.is_a?(String)
      result = answer.to_s == guess.capitalize
    else
      return Code::Incorrect
    end
  end

  puts result ? win_msg : loss_msg
  puts format_answer_time(answer_time) if timed
  puts
  return result ? Code::Win : Code::Loss
end


puts "❔ Day of the week quizz! ❔"
puts "--------------------------"
if infinite
  while true
    code = ask_question(lower_bound_year, upper_bound_year, compact, timed)
    if code == Code::Quit
      exit
    end
  end
else
  ask_question(lower_bound_year, upper_bound_year, compact, timed)
end
